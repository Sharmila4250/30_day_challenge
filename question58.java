package bitbucket;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class question58 {
public static int romanToInt(String s) {
	Map<Character,Integer>num=new HashMap();
	num.put('I', 1);
	num.put('V', 5);
	num.put('X', 10);
	num.put('L',50);
	num.put('C', 100);
	num.put('D', 500);
	num.put('M', 1000);
	int result=0;
	for(int i=0;i<s.length();i++)
    {
      char ch = s.charAt(i);      
            if(i>0 && num.get(ch) > num.get(s.charAt(i-1)))
                {
                  result += num.get(ch) - 2*num.get(s.charAt(i-1));
      }
      
      else
        result += num.get(ch);
    }
        
    return result;
 }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
System.out.println("enter the roman number");
String num=sc.next();
int result = romanToInt(num);

System.out.println("The Roman Number is: "+num);
System.out.println("Its Integer Value is: "+result);
	}

}
