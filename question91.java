package bitbucket;
//The prime factors of 13195 are 5, 7, 13 and 29. 
//What is the largest prime factor of the number 600851475143.
public class question91 {
	public static boolean checkPrime(int x) {
		boolean prime=true;
		for(int i=2;i<x;i++) {
			if(x==2) {
				prime=true;
				break;
			}
			else if(x%i==0) {
				prime=false;
				break;
			}
			else {
				prime=true;
			}
		}

		return prime;
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
long n=600851475143L;
int max=0;
for(int i=2;i<n;i++) {
	boolean ans=checkPrime(i);
	if(ans) {
		if(n%i==0) {
		max=i;
	}}
}
System.out.println("largest prime factor is "+max);
	}

}
