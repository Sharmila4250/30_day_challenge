package bitbucket;

import java.util.Scanner;

public class question90 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 int n, rev = 0;
		    Scanner sc = new Scanner(System.in);

		    System.out.println("Enter an integer to reverse");
		    n = sc.nextInt();

		    while(n != 0)
		    {
		      rev = rev * 10;
		      rev = rev + n%10;
		      n = n/10;
		    }

		    System.out.println("Reverse of a number is " + rev);
	}

}
