package bitbucket;

import java.util.Scanner;

public class question55 {
int real,image;
 public question55(int r,int i) {
	this.real=r;
	this.image=i;
}
 public void show()
 {
     System.out.print(this.real+" +i"+this.image);
 }
 public static question55 add(question55 n1 ,question55 n2) {
	 question55 res=new question55(0,0);
	 res.real=n1.real+n2.real;
	 res.image=n1.image+n2.image;
	 return res;
	 
	 
 }

	public static void main(String[] args) {
		// TODO Auto-generated method stub
	
		question55 q1=new question55(10,5);
		question55 q2=new question55(13,5);
		System.out.println("First complex number is :");
		q1.show();
		System.out.println();
		System.out.println("Second complex number is :");
		q2.show();
		System.out.println();
		question55 ans=add(q1,q2);
		System.out.println("addition is ");
		ans.show();

		
	}

}
