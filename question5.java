package bitbucket;
public class question5 {
int x, y, width, height;
public question5 (int x, int y, int width, int height) {
this.x = x;
this.y = y;
this. width = width;
this. height = height;
}
public static boolean isintersect(question5 R1 , question5 R2) {
return R1.x <= R2.x + R2.width && R1.x + R1.width >= R2.x
&& R1.y <= R2.y + R2.height && R1.y + R1.height >= R2.y;
}

public static question5 intersectRectangle(question5 R1 , question5 R2) {
if (!isintersect(R1 , R2)) {
return new question5(0, 0, -1, -1); 
}
return new question5(
Math.max(R1.x , R2.x), Math.max(R1.y , R2.y),
Math.min(R1.x + R1.width, R2.x + R2.width) - Math.max(R1.x, R2.x),
Math.min(R1.y + R1.height, R2.y + R2.height) - Math.max(R1.y , R2.y));
}
public static void display() {
	question5 r=question5.intersectRectangle(new question5(0,0,1,2), new question5(2,2,1,2));
	System.out.println(r.x+""+r.y+""+r.width+""+r.height);
}

public static void main(String[] args) {
 question5 r1=new question5(1,2,3,4);
 question5 r2=new question5(5,3,2,4);
question5 res=intersectRectangle(r1,r2);
res.display();
}}
