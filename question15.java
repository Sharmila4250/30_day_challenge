package bitbucket;

public class question15 {
	public static int rodCut(int[] price, int n)
    {
        if (n==0) {
            return 0;
        }
 
        int maxCost=0;
        for (int i=1;i<=n;i++)
        {
            int cost=price[i-1]+rodCut(price,n-i);
 
            if (cost>maxCost) {
                maxCost = cost;
            }
        }
 
        return maxCost;
    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int length[] = { 1, 2, 3, 4, 5, 6, 7, 8 };
        int price [] = { 1, 5, 8, 9, 10, 17, 17, 20 };
 
        int n = 4;
        System.out.println("Profit is " + rodCut(price, n));
	}

}
