package bitbucket;


public class question22{
public static int add(int res,int y){
  while(y!=0){
    int carry=res&y;
    res=res^y;
    y=carry<<1;
  }
  return res;
}
public static int multiply(int x,int y){
  int res=0;int k=0;
  while(x!=0){
    if((x&1)!=0){
      res=add(res,y<<k);
    }
    k=k+1;
    x=x>>1;
  }
  return res;
}
public static void main(String[] args) {

int x=0b1101;
int y=0b1001;
System.out.println("X * Y is "+multiply(x,y));

}}


