package bitbucket;

import java.util.ArrayList;
import java.util.Scanner;

public class question34 {
	static int MatrixChainOrder(int p[], int i, int j)
    {
        if (i == j)
            return 0;
  
        int min = Integer.MAX_VALUE;
        for (int k = i; k < j; k++) {
            int count = MatrixChainOrder(p, i, k) + 
                        MatrixChainOrder(p, k + 1, j) + 
                        p[i - 1] * p[k] * p[j];
  
            if (count < min)
                min = count;
        }
  
        return min;
    }
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList <Integer> max=new ArrayList<>();
Scanner sc=new Scanner(System.in);
System.out.println("How many numbers you want to store for the matrix chain multiplication?");
int n=sc.nextInt();
System.out.println("How many sequences you want to check to find the optimised one?");
int q=sc.nextInt();
int len=0;
int a[]=new int[5];
for(int i=1;i<=q;i++) {
	System.out.println("For Sequence No. "+ i);
	System.out.println("-----------------------");
	for(int j=0;j<n;j++) {
		System.out.println("enter element");
		a[j]=sc.nextInt();
		  }
	len = a.length;
	int z=MatrixChainOrder(a, 1, len - 1);
	max.add(z);
}int low=max.get(0);
int p=0;
System.out.println(max);
for(int i=0;i<max.size();i++) {
if(max.get(i)<low) {
	low=max.get(i);
	p=i+1;
}
	}
System.out.println("All the sequences with their operations are "+max+" and among them" );
System.out.println("Best sequence for the ML model that runs in least time will be the sequence no. "+ p+"and the no of operations will be "+low);
}

}
