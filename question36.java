package bitbucket;

import java.util.Scanner;

public class question36 {

	public static int catalan(int n)
    {
        int catalan[] = new int[n + 2];
         catalan[0] = 1;
        catalan[1] = 1;
        for (int i = 2; i <= n; i++) {
            catalan[i] = 0;
            for (int j = 0; j < i; j++) {
                catalan[i]
                    += catalan[j] * catalan[i-j-1];
            }
        }
         return catalan[n];
    }
 
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		System.out.println("how many numbers you want to see");
		int n=sc.nextInt();
		for (int i = 0; i < n; i++) {
            System.out.print(catalan(i) + " ");
        }
    }
}
