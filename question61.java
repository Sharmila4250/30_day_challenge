package bitbucket;

import java.util.Scanner;

public class question61 {
	static double squareRoot(double n, double l)
    {
        double x = n;
        double root;
        while (true)
        {
            root = 0.5 * (x + (n / x));
            if (Math.abs(root - x) < l) {
            break;}
            x = root;
        }
     
        return root;
    }
     
    	public static void main(String[] args) {
Scanner sc=new Scanner(System.in);
System.out.println("enter the value of n and l");
     double n = sc.nextDouble();
     double l = sc.nextDouble();
     System.out.println(squareRoot(n, l));
	}

}
