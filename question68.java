package bitbucket;

import java.util.Scanner;

public class question68 {
	public static void splitArray(int arr[], int n, int k)
    {
        for (int i = 0; i < k; i++) {
              int x = arr[0];
            for (int j = 0; j < n - 1; ++j)
                arr[j] = arr[j + 1];
            arr[n - 1] = x;
        }
    }
  
    // Driver code
    public static void main(String[] args)
    {Scanner sc=new Scanner(System.in);
        int a[] = { 12, 10, 5, 6, 52, 36 };
        int len = a.length;
        System.out.println("enter the position from where you want to split");
        int position = sc.nextInt();
  
        splitArray(a, len, position);
  
        for (int i = 0; i < len; ++i)
            System.out.print(a[i] + " ");
    }
}
