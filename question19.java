package bitbucket;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class question19 {
	public static int MatrixChainOrder(int p[], int i, int j)
    {
        if (i == j)
            return 0;
 
        int min = Integer.MAX_VALUE;
        for (int k = i; k < j; k++)
        {
            int count = MatrixChainOrder(p, i, k)
                        + MatrixChainOrder(p, k + 1, j)
                        + p[i - 1] * p[k] * p[j];
 
            if (count < min)
                min = count;
        }
         return min;
    }
 
    public static void main(String args[])
    {int min = 0;
    List<Integer> list=new ArrayList<Integer>();  

    	Scanner sc=new Scanner(System.in);
    	System.out.println("enter the size of array");
    	int n=sc.nextInt();
    	int a[]=new int[n];
    	System.out.println("enter how many array you want to enter");
    	int num=sc.nextInt();
    	for(int j=0;j<num;j++) {
    		System.out.println("for sequence "+(j+1));
    	for(int i=0;i<a.length;i++) {
        	System.out.println("enter the elements :");

    		a[i]=sc.nextInt();
    	}
    	int minseq=Integer.MAX_VALUE;
  min= MatrixChainOrder(a,1,n-1);
list.add(min);
 
    }
    	int minimum=Collections.min(list);

    		System.out.println(list);
    		System.out.println("Best sequence for the ML model that runs in least time will be the sequence no : "+(list.indexOf(minimum)+1)+" and the no of operations will be "+minimum);
    	
}}
	