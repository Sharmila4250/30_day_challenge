package bitbucket;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class question21 {

	public static List<List<Integer>> nQueen(int n) {
	        cols = new boolean[n];
	        
	        lDiagonal = new boolean[2*n];
	          
	        rDiagonal = new boolean[2*n];
	        result  = new ArrayList<>();
	        List<Integer> temp = new ArrayList<>();
	        for(int i=0;i<n;i++)temp.add(0);
	        solveNQUtil(result,n,0,temp);
	         
	        return result;
	    }
	    private static void solveNQUtil(List<List<Integer>> result,int n,int row,List<Integer> comb){
	        if(row==n){
	            result.add(new ArrayList<>(comb));
	            return;
	        }
	        for(int col = 0;col<n;col++){
	           
	            if(cols[col] || lDiagonal[row+col] || rDiagonal[row-col+n])
	                continue;
	            cols[col] = lDiagonal[row+col] = rDiagonal[row-col+n] = true;
	            comb.set(col,row+1);
	            solveNQUtil(result,n,row+1,comb);
	            cols[col] = lDiagonal[row+col] = rDiagonal[row-col+n] = false;
	        }
	    }
	  static List<List<Integer> > result
	        = new ArrayList<List<Integer> >();
	   static boolean[] cols,lDiagonal,rDiagonal;
	 
	    // Driver code
	    public static void main(String[] args)
	    {Scanner sc=new Scanner(System.in);
	    System.out.println("eneter the value of n");
	        int n = sc.nextInt();
	 
	        List<List<Integer> > res = nQueen(n);
	        System.out.println(res);
	    }
	}


