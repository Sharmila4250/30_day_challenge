package bitbucket;

public class question97 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int sum = 200;
		int[] coinSizes = { 1, 2, 5, 10, 20, 50, 100, 200 };
		int[] combinations = new int[sum + 1];
		combinations[0] = 1;

		for (int i = 0; i < coinSizes.length; i++) {
			for (int j = coinSizes[i]; j <= sum; j++) {
				combinations[j] += combinations[j - coinSizes[i]];
			}
		}

		System.out.println("number of ways are " + combinations[200]);
	}

	}


