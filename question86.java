package bitbucket;

import java.util.Scanner;
public class question86{

	static boolean kaprekarnum(int n)   
    {   
        if (n == 1)   
           return true;   
        int squaren = n * n;   
        int len = 0;   
        while (squaren != 0)   
        {   
            len++;   
            squaren /= 10;   
        }   
        squaren = n*n;   
        for (int i=1; i<len; i++)   
        {   
             int eq_parts = (int) Math.pow(10, i);   
            if (eq_parts == n)   
                continue;   
             int sum = squaren/eq_parts + squaren % eq_parts;   
             if (sum == n)   
               return true;   
        }   
        return false;   
    }       
    public static void main (String[] args)   
    {   
       for (int i=1; i<1000; i++)   
            if (kaprekarnum(i))   
                 System.out.print(i + " ");   
    }   
}   