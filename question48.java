package bitbucket;
public class question48 {
	  public static long  x_power_y(long x,int y){
	    if(y<0){
	      x=1/x;
	      y=-y;
	    }long res=1;
	    while(y!=0){
	      if((y&1)!=0){
	        res=res*x;
	      }
	      x=x*x;
	      y=y>>1;
	    }
	    return res;
	  }

	  
	  public static void main(String[] args) {

	 int x=0b10;
	 int y=0b111;
	 System.out.println("X power Y is "+x_power_y(x,y));

	}}

