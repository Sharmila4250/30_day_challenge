package bitbucket;

import java.util.Scanner;  
public class question39 {  
   public static void main(String[] args) {  
       Scanner s = new Scanner(System.in);  
       System.out.print("Enter the first number : ");  
       int first = s.nextInt();  
       System.out.print("Enter the second number : ");  
       int second = s.nextInt();  
       System.out.println("List of prime numbers between " + first + " and " + second);  
       for (int i = first; i <=second; i++) {  
           if (isPrime(i)) {  
               System.out.println(i);  
           }  
       }  
   }  
   public static boolean isPrime(int n) {  
       if (n <= 1) {  
           return false;  
       }  
       for (int i = 2; i <= Math.sqrt(n); i++) {  
           if (n % i == 0) {  
               return false;  
           }  
       }  
       return true;  
   }  
}  