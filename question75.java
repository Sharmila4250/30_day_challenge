package bitbucket;

import java.util.Scanner;

public class question75 {
	 
    
    static long doublefactorial(long n)
    {
        if (n == 0 || n==1)
            return 1;
             
        return n * doublefactorial(n - 2);
    }
 
     public static void main (String[] args)
    {
    	 Scanner sc=new Scanner(System.in);
    	 System.out.println("enter the number");
    	 int n=sc.nextInt();
        System.out.println("Double factorial"
            + " is " + doublefactorial(n));
    }
}