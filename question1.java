package bitbucket;

import java.util.Scanner;

public class question1 {
	 static int countways(int n)
	    {
	        if (n == 1) return 0;
	        if (n == 2) return 1;
	       return (n - 1) * (countways(n - 1) +
	                          countways(n - 2));
	    }
	     
	    public static void main (String[] args)
	    {
	    	Scanner sc=new Scanner(System.in);
	    	System.out.println("enter the number");
	        int n = sc.nextInt();
	        System.out.println( "no. of ways "
	                             +countways(n));
	 
	    }
	}
	 
	