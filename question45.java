package bitbucket;
public class question45 {
	  public static int swap_bit(int x,int i,int j){
if(((x>>i)&1)!=((x>>j)&1)){
int mask=(1<<i)|(1<<j);
x=x^mask;
}
return x;
}

public static int closest_integer__same_weight(int x,int n){
for(int i=0;i<n;i++){
  if(((x>>i)&1)!=((x>>(i+1))&1)){
    swap_bit(x,i,i+1);
    break;
  }
}return x;
}

public static void main(String[] args) {

 int x=0b0111;
 int n=4;

 System.out.println((closest_integer__same_weight(x,n)));
	

}}

