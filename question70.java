package bitbucket;

import java.util.Scanner;

public class question70 {
	static int MinSum(int num)
    {
        int sum = 0;
        for (int i = 2; i * i <= num; i++) {
            while (num % i == 0) {
                sum = sum+i;
                num /= i;
            }
        }
        sum += num;
        return sum;
    }
     public static void main(String[] args)
    {
    	 Scanner sc=new Scanner(System.in);
System.out.println("enter the number");  
int n=sc.nextInt();
System.out.println("minimum sum is "+MinSum(n));
    }
}
	

