package bitbucket;

import java.math.BigInteger;

public class question9 {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BigInteger fact = BigInteger.valueOf(1);
	    for (int i = 1; i <= 100; i++)
	        fact = fact.multiply(BigInteger.valueOf(i));
	    String digits = fact.toString();
	    int sum = 0;

	    for(int i = 0; i <digits.length(); i++) {
	        int digit = (int) (digits.charAt(i) - '0');
	        sum = sum + digit;
	    }

	    System.out.println(sum);
	}

}
